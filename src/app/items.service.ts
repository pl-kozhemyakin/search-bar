import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { of } from 'rxjs';
import { delay, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ItemsService {
  constructor(private http: HttpClient) {}

  // items$ = of(DATA).pipe(delay(1000));
  getItems(query?: string) {
    if (query) {
      return this.http
        .get('/assets/data.json', {
          params: {
            query,
          },
        })
        .pipe(
          delay(1000),
          map((item: any) => item.filter((i) => i.name.includes(query)))
        );
    }
    return this.http.get('/assets/data.json');
  }
}

// const DATA = [
//   {
//     id: 2080,
//     name: 'Arsenal',
//   },
//   {
//     id: 2074,
//     name: 'Aston Villa',
//   },
//   {
//     id: 2114,
//     name: 'Brighton',
//   },
//   {
//     id: 2092,
//     name: 'Burnley',
//   },
//   {
//     id: 2071,
//     name: 'Chelsea',
//   },
//   {
//     id: 2099,
//     name: 'Crystal Palace',
//   },
//   {
//     id: 2076,
//     name: 'Everton',
//   },
//   {
//     id: 2075,
//     name: 'Fulham',
//   },
//   {
//     id: 2121,
//     name: 'Leeds United',
//   },
//   {
//     id: 2122,
//     name: 'Leicester',
//   },
//   {
//     id: 2089,
//     name: 'Liverpool',
//   },
//   {
//     id: 2073,
//     name: 'Man City',
//   },
//   {
//     id: 2072,
//     name: 'Man Utd',
//   },
//   {
//     id: 2082,
//     name: 'Newcastle',
//   },
//   {
//     id: 2109,
//     name: 'Sheffield United',
//   },
//   {
//     id: 2110,
//     name: 'Southampton',
//   },
//   {
//     id: 2084,
//     name: 'Tottenham',
//   },
//   {
//     id: 2087,
//     name: 'West Bromwich Albion',
//   },
//   {
//     id: 2086,
//     name: 'West ham',
//   },
//   {
//     id: 2113,
//     name: 'Wolves',
//   },
// ];
