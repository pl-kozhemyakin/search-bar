import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListComponent } from './list/list.component';
import { SearchBarComponent } from './search-bar/search-bar.component';
import { SearchableListComponent } from './searchable-list/searchable-list.component';
import { CommonModule } from '@angular/common';
import { MaterialModule } from './material/material.module';
import { ListPipePipe } from './list/pipes/list-pipe.pipe';
import { FormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CacheInterceptor } from './core/interceptors/cache.interceptor';
@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    SearchBarComponent,
    SearchableListComponent,
    ListPipePipe
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,   
    FormsModule,
    HttpClientModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: CacheInterceptor,
    multi: true,
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
