import { Component, EventEmitter, OnDestroy, OnInit, Output } from '@angular/core';
import { fromEvent, Observable, Subscription } from 'rxjs';
import { debounceTime, map, take } from 'rxjs/operators';
import { CacheInterceptor } from '../core/interceptors/cache.interceptor';

@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.scss'],
})
export class SearchBarComponent implements OnInit, OnDestroy {
  public query: string = '';
  @Output()
  public typingEvent: EventEmitter<any> = new EventEmitter();
  public  searchBox: Element;
  public keyup$: Observable<Event>
  public searchSubscription: Subscription

  constructor(private interceptor: CacheInterceptor) {}

  ngOnInit(): void {
    this.searchBox = document.querySelector('.searchInput');
    this.keyup$ = fromEvent(this.searchBox, 'keyup');
    this.searchSubscription = this.keyup$.pipe(
      map((i:any)=> i.currentTarget.value),
      debounceTime(500)
    ).subscribe(res => {
      this.typingEvent.emit(res)
    })
  }

  search(event: any) {
  }

  ngOnDestroy(): any {
    this.searchSubscription.unsubscribe();
  }
}
