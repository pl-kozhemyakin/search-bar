import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ItemsService } from '../items.service';
const CACH_KEY = 'Mock_data'
@Component({
  selector: 'app-searchable-list',
  templateUrl: './searchable-list.component.html',
  styleUrls: ['./searchable-list.component.scss']
})
export class SearchableListComponent implements OnInit {
  public items$: Observable<any>;
  public query:string = '';

  constructor(private service: ItemsService) { }

  ngOnInit(): void {
    this.items$ = this.service.getItems();
  }

  public search(event: any): void {
    this.query = event
    this.items$ = this.service.getItems(this.query);
  }

}
